﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class zwierzeta
    {
        private int wzrost;
        public int Wzrost
        {
            get
            {
                return wzrost;
            }
            set
            {
                wzrost = value;

            }
        }
        private int waga;
        public int Waga
        {
            get
            {
                return waga;
            }
            set
            {
                waga = value;

            }
        }

        private int wiek;
        public int Wiek
        {
            get
            {
                return wiek;
            }
            set
            {
                wiek = value;

            }
        }
        private string nazwa;
        public string Nazwa
        {
            get
            {
                return nazwa;
            }
            set
            {
                nazwa = value;

            }
        }

        public zwierzeta() { }

        public zwierzeta(int wzrost, int waga, int wiek, string nazwa)
        {
            this.wzrost = wzrost;
            this.waga = waga;
            this.wiek = wiek;
            this.nazwa = nazwa;

        }

        public virtual string generetelines()
        {
            string result = string.Empty;
            result = this.nazwa + this.waga + this.wzrost + this.wiek;
            return result;
        }
    }
}
