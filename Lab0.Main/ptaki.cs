﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class ptaki : zwierzeta
    {
        private bool czy_lata;
        private bool dziob;

        public ptaki() : base() { }

        public ptaki(int wzrost, int waga, int wiek, string nazwa, bool czy_lata, bool dziob)
            : base(wzrost, waga, wiek, nazwa)
        {
            this.dziob = dziob;
            this.czy_lata = czy_lata;
        }

        public override string generetelines()
        {
            return base.generetelines() + this.czy_lata;
        }


    }
}
